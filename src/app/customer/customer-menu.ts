import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Welcome',
    icon: 'nb-compose',
    children: [
      {
        title: 'Dashboard',
        link: '/customer/welcome/dashboard'
      },
      {
        title: 'Information',
        link: '/customer/welcome/information'
      },
      {
        title: 'Promotion Report',
        link: '/customer/welcome/promotionreport'
      },
      // {
      //   title: 'Admin Permission',
      //   link: '/customer/welcome/adminpermission'
      // },
      {
        title: 'PASS Breakdown',
        link: '/customer/welcome/passbreakdown'
      },
      {
        title: 'Activation',
        link: '/customer/welcome/event'
      },
      {
        title: 'Push Notification',
        link: '/customer/welcome/pushnotification'
      },
      {
        title: 'Management Venues',
        link: '/customer/welcome/managementvenues'
      },
    ],
    home: true,
  }

  // {
  //   title: 'Dashboard',
  //   icon: 'nb-e-commerce',
  //   link: '/customer/dashboard',
  // },
  
  // {
  //   title: 'Product',
  //   icon: 'nb-compose',
  //   children: [
  //     {
  //       title: 'Sheet',
  //       link: '/customer/productv100/sheet',
  //     },
  //   ],
  // },
  
  // {
  //   title: 'IoT Dashboard',
  //   icon: 'nb-home',
  //   link: '/customer/iot-dashboard',
  // },
  // {
  //   title: 'FEATURES',
  //   group: true,
  // },
  // {
  //   title: 'Extra Components',
  //   icon: 'nb-star',
  //   children: [
  //     {
  //       title: 'Calendar',
  //       link: '/customer/extra-components/calendar',
  //     },
  //     {
  //       title: 'Stepper',
  //       link: '/customer/extra-components/stepper',
  //     },
  //     {
  //       title: 'List',
  //       link: '/customer/extra-components/list',
  //     },
  //     {
  //       title: 'Infinite List',
  //       link: '/customer/extra-components/infinite-list',
  //     },
  //     {
  //       title: 'Accordion',
  //       link: '/customer/extra-components/accordion',
  //     },
  //     {
  //       title: 'Progress Bar',
  //       link: '/customer/extra-components/progress-bar',
  //     },
  //     {
  //       title: 'Spinner',
  //       link: '/customer/extra-components/spinner',
  //     },
  //     {
  //       title: 'Alert',
  //       link: '/customer/extra-components/alert',
  //     },
  //     {
  //       title: 'Tree',
  //       link: '/customer/extra-components/tree',
  //     },
  //     {
  //       title: 'Tabs',
  //       link: '/customer/extra-components/tabs',
  //     },
  //     {
  //       title: 'Calendar Kit',
  //       link: '/customer/extra-components/calendar-kit',
  //     },
  //     {
  //       title: 'Chat',
  //       link: '/customer/extra-components/chat',
  //     },
  //   ],
  // },
  // {
  //   title: 'Forms',
  //   icon: 'nb-compose',
  //   children: [
  //     {
  //       title: 'Form Inputs',
  //       link: '/customer/forms/inputs',
  //     },
  //     {
  //       title: 'Form Layouts',
  //       link: '/customer/forms/layouts',
  //     },
  //     {
  //       title: 'Buttons',
  //       link: '/customer/forms/buttons',
  //     },
  //     {
  //       title: 'Datepicker',
  //       link: '/customer/forms/datepicker',
  //     },
  //   ],
  // },
  // {
  //   title: 'UI Features',
  //   icon: 'nb-keypad',
  //   link: '/customer/ui-features',
  //   children: [
  //     {
  //       title: 'Grid',
  //       link: '/customer/ui-features/grid',
  //     },
  //     {
  //       title: 'Icons',
  //       link: '/customer/ui-features/icons',
  //     },
  //     {
  //       title: 'Typography',
  //       link: '/customer/ui-features/typography',
  //     },
  //     {
  //       title: 'Animated Searches',
  //       link: '/customer/ui-features/search-fields',
  //     },
  //   ],
  // },
  // {
  //   title: 'Modal & Overlays',
  //   icon: 'nb-layout-default',
  //   children: [
  //     {
  //       title: 'Dialog',
  //       link: '/customer/modal-overlays/dialog',
  //     },
  //     {
  //       title: 'Window',
  //       link: '/customer/modal-overlays/window',
  //     },
  //     {
  //       title: 'Popover',
  //       link: '/customer/modal-overlays/popover',
  //     },
  //     {
  //       title: 'Toastr',
  //       link: '/customer/modal-overlays/toastr',
  //     },
  //     {
  //       title: 'Tooltip',
  //       link: '/customer/modal-overlays/tooltip',
  //     },
  //   ],
  // },
  // {
  //   title: 'Bootstrap',
  //   icon: 'nb-gear',
  //   children: [
  //     {
  //       title: 'Form Inputs',
  //       link: '/customer/bootstrap/inputs',
  //     },
  //     {
  //       title: 'Buttons',
  //       link: '/customer/bootstrap/buttons',
  //     },
  //     {
  //       title: 'Modal',
  //       link: '/customer/bootstrap/modal',
  //     },
  //   ],
  // },
  // {
  //   title: 'Maps',
  //   icon: 'nb-location',
  //   children: [
  //     {
  //       title: 'Google Maps',
  //       link: '/customer/maps/gmaps',
  //     },
  //     {
  //       title: 'Leaflet Maps',
  //       link: '/customer/maps/leaflet',
  //     },
  //     {
  //       title: 'Bubble Maps',
  //       link: '/customer/maps/bubble',
  //     },
  //     {
  //       title: 'Search Maps',
  //       link: '/customer/maps/searchmap',
  //     },
  //   ],
  // },
  // {
  //   title: 'Charts',
  //   icon: 'nb-bar-chart',
  //   children: [
  //     {
  //       title: 'Echarts',
  //       link: '/customer/charts/echarts',
  //     },
  //     {
  //       title: 'Charts.js',
  //       link: '/customer/charts/chartjs',
  //     },
  //     {
  //       title: 'D3',
  //       link: '/customer/charts/d3',
  //     },
  //   ],
  // },
  // {
  //   title: 'Editors',
  //   icon: 'nb-title',
  //   children: [
  //     {
  //       title: 'TinyMCE',
  //       link: '/customer/editors/tinymce',
  //     },
  //     {
  //       title: 'CKEditor',
  //       link: '/customer/editors/ckeditor',
  //     },
  //   ],
  // },
  // {
  //   title: 'Tables',
  //   icon: 'nb-tables',
  //   children: [
  //     {
  //       title: 'Smart Table',
  //       link: '/customer/tables/smart-table',
  //     },
  //   ],
  // },
  // {
  //   title: 'Miscellaneous',
  //   icon: 'nb-shuffle',
  //   children: [
  //     {
  //       title: '404',
  //       link: '/customer/miscellaneous/404',
  //     },
  //   ],
  // },
  // {
  //   title: 'Auth',
  //   icon: 'nb-locked',
  //   children: [
  //     {
  //       title: 'Login',
  //       link: '/auth/login',
  //     },
  //     {
  //       title: 'Register',
  //       link: '/auth/register',
  //     },
  //     {
  //       title: 'Request Password',
  //       link: '/auth/request-password',
  //     },
  //     {
  //       title: 'Reset Password',
  //       link: '/auth/reset-password',
  //     },
  //   ],
  // },
];
