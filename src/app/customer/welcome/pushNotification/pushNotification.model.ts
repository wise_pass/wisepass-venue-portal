
export interface ReponseDataList<T> {
    message: string;
    statusCode: number;
    data: Array<T>;
}

export interface PushNotificationModel {
    title: string;
    body: string;
}


export interface ReponseData<T> {
    message: string;
    statusCode: number;
    data: T;
}

export interface PushNotificationDetailModel {
    pushNotificationDetail : PushNotificationDetail[];
}

export interface PushNotificationDetail{
    title: string;
    body: string;
    createdDate: string;
    type: string;
}
