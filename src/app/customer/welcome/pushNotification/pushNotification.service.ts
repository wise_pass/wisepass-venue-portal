import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/observable/of';
import { ConfigService } from '../../../configuration/configuration';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class PushNotificationService {
    constructor(private http: HttpClient, public _configService: ConfigService) {}

    httpOptions = {
        headers: {},
    };

    getDecodedAccessToken(token: string): any {
        try {
            return jwt_decode(token);
        }
        catch (Error) {
            return null;
        }
    }

    initHttpHeader() {
        this.httpOptions.headers = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Authorization': (localStorage.getItem('token') ? localStorage.getItem('token') : ''),
            },
        );
    }

    pushNotification(title, body) {
        this.initHttpHeader();
        var pushNotificatino = {
            "title" : title,
            "body" : body,
        }
        return this.http.post(
            this._configService.getAPILink('pushNotification').toString(),
            pushNotificatino,
            this.httpOptions);
    }

    getPushNotification() {
        this.initHttpHeader();
        return this.http.get(
            this._configService.getAPILink('getPushNotification').toString(),
            this.httpOptions);
    }
}
