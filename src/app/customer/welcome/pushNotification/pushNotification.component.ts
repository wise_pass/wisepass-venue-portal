import { Component, OnInit, OnChanges } from '@angular/core';
import { PushNotificationService } from './pushNotification.service';
import { PushNotificationModel, ReponseDataList, PushNotificationDetailModel, PushNotificationDetail, ReponseData } from './pushNotification.model';
import { NbToastrService } from '../../../../../node_modules/@nebular/theme/components/toastr/toastr.service';
import { Router } from '../../../../../node_modules/@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-push-notification',
  styleUrls: ['./pushNotification.component.scss'],
  templateUrl: './pushNotification.component.html',
})
export class PushNotificationComponent implements OnInit, OnChanges {

  loading: boolean;
  title: string;
  body: string;
  htmlStr: string;
  listNotification: PushNotificationDetail[];

  constructor(
    private _pushNotificationService: PushNotificationService,
    private toastrService: NbToastrService,
    private _router: Router,
    private datePipe: DatePipe
  ) { }

  settings = {
    columns: {
      title: {
        title: 'Title',
        filter: true,
      },
      body: {
        title: 'Description',
        filter: true,
      },
      createdDate: {
        title: 'DateTime', sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          var raw = new Date(date);
          var formatted = this.datePipe.transform(raw, 'dd MMM yyyy');
          return formatted;
        }
      },
    },
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: false,
    },
    pager: {
      display: true,
      perPage: 20
    },

  };

  ngOnInit() {
    this.getPushNotification();
  }

  ngOnChanges() {
  }

  onPushNotification() {
    if (!this.title || !this.title.trim() || !this.body || !this.body.trim()) {
      this.toastrService.warning('', 'Title and Body are required', { duration: 3000 });
      return;
    }
    this._pushNotificationService.pushNotification(this.title, this.body)
      .subscribe(
        data => {
          try {
            const response = data as ReponseDataList<PushNotificationModel>;
            // console.log('Call API Push Notification', response);
            if (response.statusCode == 200) {
              this.toastrService.success('Success', this.title, { duration: 3000 });
              this.title = "";
              this.body = "";
              this.getPushNotification();
            }
          } catch (error) {
            console.log("errors", error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );
  }

  getPushNotification() {
    this.loading = true;
    this._pushNotificationService.getPushNotification()
      .subscribe(
        data => {
          try {
            const response = data as ReponseData<PushNotificationDetailModel>;
            this.listNotification = response.data.pushNotificationDetail;
            // console.log('Call API Get User Push Notification', this.listNotification);

          } catch (error) {
            console.log("errors", error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );
  }
}

