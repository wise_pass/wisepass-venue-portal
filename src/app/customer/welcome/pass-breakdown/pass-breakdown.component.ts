import { Component, OnInit } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { PassBreakDownService } from './pass-breakdown.service';
import { ResponseData, BusinessConsumptionList, BusinessConsumptionModel, BusinessVenueMonthConsumptionModel } from './pass-breakdown.model';

@Component({
  selector: 'ngx-pass-breakdown',
  styleUrls: ['./pass-breakdown.component.scss'],
  templateUrl: './pass-breakdown.component.html',
})
export class PassBreakDownComponent implements OnInit {

  userId: string;
  listmonthConsumption: BusinessConsumptionModel[];
  listBusinessConsumption = [];
  listVenueConsumption: BusinessVenueMonthConsumptionModel[];
  venues = [];
  dateArray = [];
  isValid: boolean = true;
  htmlStr: string;
  selectedItemBusiness: string = "Pernod Ricard Vietnam";
  loading: boolean;

  settings = {
    columns: {
      numericalOrder: {
        title: 'STT',
        width: '5%'
      },
      businessVenueName: {
        title: 'Venue'
      },
      businessVenueTypeName: {
        title: 'Category'
      },
      city: {
        title: 'City'
      },
      totalMembership: {
        title: 'Consumers'
      },
      totalPass: {
        title: 'PASS'
      },
      month: {
        title: 'Month'
      },
    },
    actions: false,
    pager: {
      display: true,
      perPage: 20
    },
  };

  constructor(
    private _passBreakDownService: PassBreakDownService,
  ) { }

  ngOnInit() {
    this.onVenueMonthSubmit();
  }

  onVenueMonthSubmit() {
    try {
      const infoJwt = jwt_decode(localStorage.getItem('token') ? localStorage.getItem('token') : '');
      if (infoJwt) {
        this.userId = infoJwt.UserId;
      }
    } catch (e) {

    }

    this.loading = true;
    this._passBreakDownService.getPassBreakDown()
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<BusinessConsumptionList>;
            this.listmonthConsumption = response.data.businessConsumptionList;
            console.log('Call API Get Venue Month Consumpiton', this.listmonthConsumption);
            if (this.listmonthConsumption && this.listmonthConsumption.length > 0) {
              this.selectedItemBusiness = this.listmonthConsumption[0].businessId;
              this.listmonthConsumption.forEach(venueItem => {
                this.listVenueConsumption = venueItem.businessVenueMonthConsumptionList;
              });
              this.htmlStr = '';
            } else {
              this.htmlStr = '<h3 class="text-center">Not data</h3>';
            }
          } catch (error) {
            console.log('errors', error);
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );
  }
}

