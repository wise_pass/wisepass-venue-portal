import { Component, OnInit, OnChanges } from '@angular/core';
import { UserDashboardService } from '../userdashboard.service';
import { ResponseData, PassSupplierConsumerByBusinessList, PassSupplierConsumerByBusinessModel, PassSupplierConsumerByMonthList, PassSupplierConsumerByWeekList } from '../userdashboard.model';

@Component({
  selector: 'ngx-pass-in-period',
  styleUrls: ['./pass-in-period.component.scss'],
  templateUrl: './pass-in-period.component.html',
})
export class PassInPeriodComponent implements OnInit, OnChanges {

  selectedItemPeriod: string = 'month';
  selectedItem: string = 'month';
  echartsIntance: any;
  options: any;
  revealed = false;
  passSupplier: PassSupplierConsumerByBusinessModel[];
  passSupplierConsumerByMonthList: PassSupplierConsumerByMonthList[] = [];
  passSupplierConsumerByWeekList: PassSupplierConsumerByWeekList[] = [];
  arrTotalInMonth: PassSupplierConsumerByMonthList[] = [];
  arrTotalInWeek: PassSupplierConsumerByWeekList[] = [];
  arrTotalPassInMonthChart: PassSupplierConsumerByMonthList[] = [];
  arrTotalPassInWeekChart: PassSupplierConsumerByWeekList[] = [];


  toggleView() {
    this.revealed = !this.revealed;
  }

  constructor(
    private _userDashboardService: UserDashboardService,
  ) { }

  ngOnInit() {
    this.getPassInPeriod();
    this.options = {
      "grid": {
        "left": 0,
        "top": 0,
        "right": 0,
        "bottom": 0,
        "containLabel": true
      },
      "xAxis": {
        "type": "category",
        "data": [],
        "axisLabel": {
          "color": "#a1a1e5",
          "fontSize": "12"
        },
        "axisLine": {
          "show": false
        },
        "axisTick": {
          "show": false
        }
      },
      "yAxis": {
        "show": false,
        "axisLine": {
          "show": false
        },
        "axisLabel": {
          "show": false
        },
        "axisTick": {
          "show": false
        },
        "boundaryGap": [
          0,
          "5%"
        ]
      },
      "tooltip": {
        "axisPointer": {
          "type": "shadow"
        },
        "textStyle": {
          "color": "#ffffff",
          "fontWeight": "normal",
          "fontSize": 16
        },
        "position": "top",
        "backgroundColor": "rgba(0, 255, 170, 0.35)",
        "borderColor": "#00d977",
        "borderWidth": 3,
        "formatter": "{c0} PASS",
        "extraCssText": "box-shadow: 0px 2px 46px 0 rgba(0, 255, 170, 0.35); border-radius: 10px; padding: 4px 16px;"
      },
      "series": [
        {
          "type": "bar",
          "barWidth": "40%",
          "data": [],
          "itemStyle": {
            "normal": {
              "color": {
                "x": 0,
                "y": 0,
                "x2": 0,
                "y2": 1,
                "type": "linear",
                "global": false,
                "colorStops": [
                  {
                    "offset": 0,
                    "color": "#fc0"
                  },
                  {
                    "offset": 1,
                    "color": "#ffa100"
                  }
                ]
              },
              "opacity": 1,
              "shadowColor": "#fc0",
              "shadowBlur": "5"
            }
          }
        }
      ]
    };
  }
  ngOnChanges() {
    if (this.passSupplier) {
      this.onPeriodChanged();
      this.onChartChanged();
    }
  }

  getPassInPeriod() {
    this._userDashboardService.getPassInPeriod()
      .subscribe(
        data => {
          try {
            const reponse = data as ResponseData<PassSupplierConsumerByBusinessList>;
            this.passSupplier = reponse.data.passSupplierConsumerByBusinessList;
            console.log("call API passSupplier", this.passSupplier);
            if (this.passSupplier && this.passSupplier.length > 0) {
              this.passSupplier.forEach(arrPeriod => {
                this.passSupplierConsumerByMonthList = arrPeriod.passSupplierConsumerByMonthList;
                this.passSupplierConsumerByWeekList = arrPeriod.passSupplierConsumerByWeekList;
                
                this.onPeriodChanged();
                this.onChartChanged();
              });
              
            } 
          } catch {
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        }, );
  }

  onPeriodChanged() {
    if (this.selectedItem === 'week') {
      this.arrTotalInWeek = this.passSupplierConsumerByWeekList;
    } else if (this.selectedItem === 'month') {
      this.arrTotalInMonth = this.passSupplierConsumerByMonthList;
    } else {
      console.log("error select item");
    }
  };

  onChartChanged() {
    if (this.selectedItemPeriod === 'week') {
      this.arrTotalPassInWeekChart = this.passSupplierConsumerByWeekList;
      this.onDrawing(this.arrTotalPassInWeekChart);
    } else if (this.selectedItemPeriod === 'month') {
      this.arrTotalPassInMonthChart = this.passSupplierConsumerByMonthList;
      this.onDrawing(this.arrTotalPassInMonthChart);
    } else {
      console.log("error select item");
    }
  }


  onDrawing(arrPeriodDetail) {
    if (this.echartsIntance) {
      let arrXaxis = [];
      let arrSerie = [];
      arrPeriodDetail.forEach(detailItem => {
        if (detailItem.month) {
          arrXaxis.push(detailItem.month);
          arrSerie.push(detailItem.totalPassInMonth);
        } else if (detailItem.week) {
          arrXaxis.push(detailItem.week);
          arrSerie.push(detailItem.totalPassInWeek);
        } else {
          console.log('error drawing');
        }
      });
      
      this.options.xAxis.data = arrXaxis;
      this.options.series[0].data = arrSerie;
      this.echartsIntance.setOption(this.options);
    }
  }

  onChartInit(echarts) {
    this.echartsIntance = echarts;
  }
}
