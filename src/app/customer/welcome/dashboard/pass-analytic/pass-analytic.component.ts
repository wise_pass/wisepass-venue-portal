import { Component, OnInit } from '@angular/core';
import { UserDashboardService } from '../userdashboard.service';
import { ResponseData, ConsumersAndPassByBusinessList, ConsumersAndPassByBusinessModel } from '../userdashboard.model';

@Component({
  selector: 'ngx-pass-analytic',
  styleUrls: ['./pass-analytic.component.scss'],
  templateUrl: './pass-analytic.component.html',
})
export class PassAnalyticComponent implements OnInit {

  echartsIntance: any;
  options: any;
  business: ConsumersAndPassByBusinessModel[];

  constructor(
    private _userDashboardService: UserDashboardService,
  ) { }

  ngOnInit() {
    this.getPassAnalytic();
    this.getOptions();

  }

  getPassAnalytic() {
    this._userDashboardService.getConsumersAndPass()
      .subscribe(
        data => {
          try {
            const reponse = data as ResponseData<ConsumersAndPassByBusinessList>;
            this.business = reponse.data.consumersAndPassByBusinessList;
            // console.log("call API passSupplier", this.business);
            if (this.business && this.business.length > 0) {
              this.onDrawing();
            }
          } catch {
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        }, );
  }

  getOptions() {
    this.options = {
      "grid": {
        "left": 40,
        "top": 20,
        "right": 20,
        "bottom": 60
      },
      "tooltip": {
        "trigger": "axis",
        show: true,
        formatter: `<div style="display: flex; align-items: center;"><div style="background: linear-gradient(90deg, rgb(0, 136, 255) 0%, rgb(61, 175, 255) 100%);
        width: 20px;height: 20px;border-radius: .2rem; margin-right: 5px"></div> PASS: {c0}</div>
        <div style="display: flex; align-items: center;"><div style="background: linear-gradient(90deg, rgb(164, 84, 255) 0%, rgb(118, 89, 255) 100%);
        width: 20px;height: 20px;border-radius: .2rem;  margin-right: 5px"></div> Consumers: {c1}</div>`,
        "axisPointer": {
          "type": "line",
          "lineStyle": {
            "color": "rgba(255, 255, 255, 0.1)",
            "width": "1"
          },
        },
        "textStyle": {
          "color": "#ffffff",
          "fontSize": 20,
          "fontWeight": "normal"
        },
        "position": "top",
        "backgroundColor": "rgba(0, 255, 170, 0.35)",
        "borderColor": "#00d977",
        "borderWidth": 3,
        "extraCssText": "box-shadow: 0px 2px 46px 0 rgba(0, 255, 170, 0.35); border-radius: 10px; padding: 8px 24px;"
      },
      "xAxis": {
        "type": "category",
        "boundaryGap": false,
        "offset": 25,
        "data": [],
        "axisTick": {
          "show": false
        },
        "axisLabel": {
          "color": "#a1a1e5",
          "fontSize": "16"
        },
        "axisLine": {
          "lineStyle": {
            "color": "rgba(161, 161 ,229, 0.3)",
            "width": "2"
          }
        }
      },
      "yAxis": [
        {
        "type": "value",
        "boundaryGap": false,
        "axisLine": {
          "lineStyle": {
            "color": "rgba(161, 161 ,229, 0.3)",
            "width": "1"
          }
        },
        "axisLabel": {
          "color": "#a1a1e5",
          "fontSize": "16"
        },
        "axisTick": {
          "show": false
        },
        "splitLine": {
          "lineStyle": {
            "color": "rgba(161, 161 ,229, 0.2)",
            "width": "1"
          }
        }
      },
      {
        "type": "value",
        "boundaryGap": false,
        "axisLine": {
          "lineStyle": {
            "color": "rgba(161, 161 ,229, 0.3)",
            "width": "1"
          }
        },
        "axisLabel": {
          "color": "#a1a1e5",
          "fontSize": "16"
        },
        "axisTick": {
          "show": false
        },
        "splitLine": {
          "lineStyle": {
            "color": "rgba(161, 161 ,229, 0.2)",
            "width": "1"
          }
        }
      },
    ],
      
      "series": [
        {
          "type": "line",
          "smooth": true,
          "symbolSize": 20,
          "tooltip": {
            "show": true,
            "extraCssText": "",

          },
          "itemStyle": {
            "normal": {
              "opacity": 0
            },
            "emphasis": {
              "opacity": 0
            }
          },
          "lineStyle": {
            "normal": {
              "width": "1",
              "type": "solid",
              "color": {
                "x": 0,
                "y": 0,
                "x2": 0,
                "y2": 1,
                "type": "linear",
                "global": false,
                "colorStops": []
              }
            }
          },
          "areaStyle": {
            "normal": {
              "color": {
                "x": 0,
                "y": 0,
                "x2": 0,
                "y2": 1,
                "type": "linear",
                "global": false,
                "colorStops": [
                  {
                    "offset": 0,
                    "color": "rgba(59, 165, 243, 1)"
                  },
                  {
                    "offset": 1,
                    "color": "rgba(4, 133, 243 , 1)"
                  }
                ]
              },
              "opacity": 1
            }
          },
          "data": []
        },
        {
          "type": "line",
          "smooth": true,
          "symbolSize": 20,
          "itemStyle": {
            "normal": {
              "opacity": 0
            },
            "emphasis": {
              "color": "#ffffff",
              "borderColor": "#ffffff",
              "borderWidth": 2,
              "opacity": 1
            }
          },
          "lineStyle": {
            "normal": {
              "width": "6",
              "type": "dotted",
              "color": {
                "x": 0,
                "y": 0,
                "x2": 0,
                "y2": 1,
                "type": "linear",
                "global": false,
                "colorStops": [
                  {
                    "offset": 0,
                    "color": "#ffffff"
                  },
                  {
                    "offset": 1,
                    "color": "#ffffff"
                  }
                ]
              },
              "shadowColor": "rgba(14, 16, 48, 0.4)",
              "shadowBlur": 6,
              "shadowOffsetY": 12
            }
          },
          "areaStyle": {
            "normal": {
              "color": {
                "x": 0,
                "y": 0,
                "x2": 0,
                "y2": 1,
                "type": "linear",
                "global": false,
                "colorStops": [
                  {
                    "offset": 0,
                    "color": "rgba(188, 92, 255, 1)"
                  },
                  {
                    "offset": 1,
                    "color": "rgba(188, 92, 255, 0.5)"
                  }
                ]
              }
            }
          },
          "data": []
        }
      ]
    };
  }

  onDrawing() {
    if (this.business) {
      let arrBusinessVenue = this.business;
      let arrXaxis = [];
      let arrSeriesPass = [];
      let arrSeriesUniquePass = [];

      (arrBusinessVenue || []).forEach(bvItem => {
        (bvItem.totalConsumersAndPassList || []).forEach(passItem => {
          arrXaxis.push(passItem.month);
          arrSeriesPass.push(passItem.totalPassInMonth);
          arrSeriesUniquePass.push(passItem.totalConsumerInMonth);
        });
      });

      this.options.xAxis.data = arrXaxis;

      this.options.series.forEach(seriesItem => {
        seriesItem.data = arrSeriesPass;
      });
      this.options.series[0].data = arrSeriesPass;
      this.options.series[1].data = arrSeriesUniquePass;

      this.echartsIntance.setOption(this.options);
    }
  }

  onChartInit(echarts) {
    this.echartsIntance = echarts;
  }
}
