import { Component, OnInit } from '@angular/core';
import { UserDashboardService } from '../userdashboard.service';
import { UserDashboardByVenueModel, ResponseData, ConsumersAndPassByBusinessList } from '../userdashboard.model';

@Component({
  selector: 'ngx-consumer',
  styleUrls: ['./consumer.component.scss'],
  templateUrl: './consumer.component.html',
})
export class ConsumerComponent implements OnInit  {

  constructor(
    private _userDashboardService: UserDashboardService,
  ) { }

  businesss: UserDashboardByVenueModel[];
  totalConsumers : number = 0;
  isDashboardShown = true;
  htmlStr: string = "";

  ngOnInit() {
    this.getAPI();
  }

  getAPI() {
    this._userDashboardService.getConsumersAndPass()
      .subscribe(
        data => {
          try {
            const reponse = data as ResponseData<ConsumersAndPassByBusinessList>;
            this.businesss = reponse.data.consumersAndPassByBusinessList;
            if (this.businesss && this.businesss.length > 0) {
              this.businesss.forEach(businessItem => {
                this.totalConsumers = businessItem.totalConsumers;
              });
            }
          } catch {
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );
  }
}
