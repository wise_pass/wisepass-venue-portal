
export interface ResponseData<T> {
    message: string;
    statusCode: number;
    data: T;
}

// consumer 
export interface UserDashboardByVenueList {
    userDashboardByVenueList: Array<UserDashboardByVenueModel>
}

export interface UserDashboardByVenueModel {
    businessId: string;
    businessName: string;
    totalConsumers: number;
    totalPass: number;
}
// pass in period
export interface PassSupplierConsumerByBusinessList {
    passSupplierConsumerByBusinessList: Array<PassSupplierConsumerByBusinessModel>
}

export interface PassSupplierConsumerByBusinessModel {
    businessId: string;
    businessName: string;
    passSupplierConsumerByMonthList: PassSupplierConsumerByMonthList[];
    passSupplierConsumerByWeekList: PassSupplierConsumerByWeekList[];
}

export interface PassSupplierConsumerByMonthList {
    year: number;
    month: string;
    totalPassInMonth: number;
    precentInMonth: number;
}

export interface PassSupplierConsumerByWeekList {
    week: string;
    totalPassInWeek: number;
    precentInWeek: number;
}

// pass analytics
export interface PassAnalyticsByBusinessList {
    passAnalyticsByBusinessList: Array<PassAnalyticsByBusinessModel>
}

export interface PassAnalyticsByBusinessModel {
    businessId: string;
    businessName: string;
    passAnalyticsByPeriodList: PassAnalyticsByPeriodList[];
}

export interface PassAnalyticsByPeriodList {
    month: string;
    totalPassInMonth: number;
    totalConsumerInMonth: number;
    totalNewPassInMonth: number;
    totalRepertPassInMonth: number;
}

// consumers and pass
export interface ConsumersAndPassByBusinessList {
    consumersAndPassByBusinessList: Array<ConsumersAndPassByBusinessModel>
}

export interface ConsumersAndPassByBusinessModel {
    businessId: string;
    businessName: string;
    totalPass: number;
    totalConsumers: number;
    totalConsumersAndPassList: TotalConsumersAndPassList[];
}

export interface TotalConsumersAndPassList {
    month: string;
    totalPassInMonth: number;
    totalConsumerInMonth: number;
    totalNewConsumers: number;
    totalRepeatConsumers: number;
    totalNewPass: number;
    totalRepeatPass: number;
}

// dashboard
export interface DashboardModel {
    dashboardDetailList: Array<DashboardDetailModel>
}

export interface DashboardDetailModel {
    totalActiveLocation: number;
    activeLocationHaNoiCount: number;
    activeLocationHoChiMinhCount: number;
    membersUseStarterPacksCount: number;
}