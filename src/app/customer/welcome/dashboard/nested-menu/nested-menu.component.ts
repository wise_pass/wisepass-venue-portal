import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { DashboardModel, Venue, Business } from '../../welcom.model';

@Component({
  selector: 'ngx-nested-menu',
  styleUrls: ['./nested-menu.component.scss'],
  templateUrl: './nested-menu.component.html',
})
export class NestedMenuComponent implements OnInit, OnChanges {
  @Input() dashboardModel: DashboardModel;

  business: Business;
  venues: Venue[] = [];
  venueId: string;
  venueName: string;
  selectedItem: string;


  ngOnInit() {
  }
  ngOnChanges() {
    // if (this.dashboardModel) {
    //   this.showArrVenue();
    //   // this.onPeriodChanged();
    // }
  }

  // private showArrVenue() {
  //   this.venues = this.dashboardModel.data.business[0].venues;
  //   this.venues.forEach(venueItem => {
  //     this.venueId = venueItem.id;
  //     this.venueName = venueItem.name;
  //   });
  // }
  // onPeriodChanged(){
  //   console.log("fe");
  //   // let value = event.target.value;
  // }
}
