import { Component, OnInit } from '@angular/core';
import { UserDashboardService } from '../userdashboard.service';
import { ResponseData, DashboardModel, DashboardDetailModel } from '../userdashboard.model';
import { Router } from '../../../../../../node_modules/@angular/router';

@Component({
  selector: 'ngx-pass-activity',
  styleUrls: ['./pass-activity.component.scss'],
  templateUrl: './pass-activity.component.html',
})
export class PassActivityComponent implements OnInit {

  activeLocationHCMCount: number = 0;
  activeLocationHNCount: number = 0;
  activeLocationHCMPercent: number = 0;
  activeLocationHNPercent: number = 0;
  dashboardDetailList: DashboardDetailModel[];

  constructor(
    private _userDashboardService: UserDashboardService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.getAPI();
  }

  getAPI() {
    this._userDashboardService.getDashboard()
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<DashboardModel>;
            this.dashboardDetailList = response.data.dashboardDetailList;
            // console.log('Call API Dashboard', this.dashboardDetailList);
            if (this.dashboardDetailList && this.dashboardDetailList.length > 0) {
              this.dashboardDetailList.forEach(item => {
                this.activeLocationHCMCount = item.activeLocationHoChiMinhCount;
                this.activeLocationHCMPercent = Math.round((this.activeLocationHCMCount / 40) * 100);
                this.activeLocationHNCount = item.activeLocationHaNoiCount;
                this.activeLocationHNPercent = Math.round((this.activeLocationHNCount / 6) * 100);
              });
            }
          } catch {
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        },
    );
  }
}
