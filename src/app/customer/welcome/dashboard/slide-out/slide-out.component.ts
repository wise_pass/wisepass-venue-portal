import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { UserDashboardService } from '../userdashboard.service';
import { ResponseData, ConsumersAndPassByBusinessList, ConsumersAndPassByBusinessModel, TotalConsumersAndPassList } from '../userdashboard.model';

@Component({
  selector: 'ngx-slide-out',
  styleUrls: ['./slide-out.component.scss'],
  templateUrl: './slide-out.component.html',
})
export class SlideOutComponent implements OnInit, OnChanges {
  @Input() showVisitorsStatistics: boolean = false;

  options: any;
  echartsIntance: any;
  passAnalyticsByBusiness: ConsumersAndPassByBusinessModel[] = [];
  arrPass: TotalConsumersAndPassList[] = [];
  arrPassCureentMonth;
  newPass: number;
  repeatPass: number;
  repeatPassPercent: number;
  newPassPercent: number;
  totalPassPercent: number;
  currentMonth: string = "";

  constructor(
    private _userDashboardService: UserDashboardService,
  ) { }

  toggleStatistics() {
    this.showVisitorsStatistics = !this.showVisitorsStatistics;
  }

  ngOnInit() {
    this.getNewAndRepeatPass();
  }

  ngOnChanges() {
    this.getOption();
  }

  getNewAndRepeatPass() {
    this._userDashboardService.getConsumersAndPass()
      .subscribe(
        data => {
          try {
            const reponse = data as ResponseData<ConsumersAndPassByBusinessList>;
            this.passAnalyticsByBusiness = reponse.data.consumersAndPassByBusinessList;

            var now = new Date();
            const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
              'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
            ];

            this.currentMonth = monthNames[now.getMonth()];

            if (this.passAnalyticsByBusiness && this.passAnalyticsByBusiness.length > 0) {
              this.passAnalyticsByBusiness.forEach(businesss => {
                this.arrPass = businesss.totalConsumersAndPassList;
                if (this.arrPass && this.arrPass.length > 0) {
                  this.arrPassCureentMonth = this.arrPass.find(x => x.month === this.currentMonth);
                  this.onDrawing();
                }
              });
            }
          } catch {
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        }, );
  }

  getOption() {
    this.options = {
      "tooltip": {
        "trigger": "item",
        "formatter": ""
      },
      "series": [
        {
          "name": " ",
          "clockWise": true,
          "hoverAnimation": false,
          "type": "pie",
          "center": [
            "50%",
            "50%"
          ],
          "radius": [
            "70%",
            "90%"
          ],
          "data": [
            {
              "value": " ",
              "name": " ",
              "label": {
                "normal": {
                  "position": "center",
                  "formatter": "",
                  "textStyle": {
                    "fontSize": "22",
                    "fontFamily": "\"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, sans-serif",
                    "fontWeight": "600",
                    "color": "#ffffff"
                  }
                }
              },
              "tooltip": {
                "show": false
              },
              "itemStyle": {
                "normal": {
                  "color": {
                    "x": 0,
                    "y": 0,
                    "x2": 0,
                    "y2": 1,
                    "type": "linear",
                    "global": false,
                    "colorStops": [
                      {
                        "offset": 0,
                        "color": "#7bff24"
                      },
                      {
                        "offset": 1,
                        "color": "#2ec7fe"
                      }
                    ]
                  },
                  "shadowColor": "#19977E",
                  "shadowBlur": 0,
                  "shadowOffsetX": 0,
                  "shadowOffsetY": 3
                }
              },
              "hoverAnimation": false
            },
            {
              "value": "",
              "name": " ",
              "tooltip": {
                "show": false
              },
              "label": {
                "normal": {
                  "position": "inner"
                }
              },
              "itemStyle": {
                "normal": {
                  "color": "#2f296b"
                }
              }
            }
          ]
        },
        {
          "name": " ",
          "clockWise": true,
          "hoverAnimation": false,
          "type": "pie",
          "center": [
            "50%",
            "50%"
          ],
          "radius": [
            "60%",
            "95%"
          ],
          "data": [
            {
              "value": " ",
              "name": " ",
              "label": {
                "normal": {
                  "position": "center",
                  "formatter": "",
                  "textStyle": {
                    "fontSize": "22",
                    "fontFamily": "\"Segoe UI\", Roboto, \"Helvetica Neue\", Arial, sans-serif",
                    "fontWeight": "600",
                    "color": "#ffffff"
                  }
                }
              },
              "tooltip": {
                "show": false
              },
              "itemStyle": {
                "normal": {
                  "color": {
                    "x": 0,
                    "y": 0,
                    "x2": 0,
                    "y2": 1,
                    "type": "linear",
                    "global": false,
                    "colorStops": []
                  }
                }
              },
              "hoverAnimation": false
            },
            {
              "value": " ",
              "name": " ",
              "tooltip": {
                "show": false
              },
              "label": {
                "normal": {
                  "position": "inner"
                }
              },
              "itemStyle": {
                "normal": {
                  "color": {
                    "x": 0,
                    "y": 0,
                    "x2": 0,
                    "y2": 1,
                    "type": "linear",
                    "global": false,
                    "colorStops": [
                      {
                        "offset": 0,
                        "color": "#ff894a"
                      },
                      {
                        "offset": 1,
                        "color": "#ffcc10"
                      }
                    ]
                  },
                  "shadowColor": "#cf7c1c",
                  "shadowBlur": 0,
                  "shadowOffsetX": "0",
                  "shadowOffsetY": "3"
                }
              }
            }
          ]
        }
      ]
    };
  }

  onDrawing() {
    if (this.arrPassCureentMonth) {
      this.repeatPassPercent = 0;
      this.newPassPercent = 0;

      this.arrPass.forEach(passItem => {
        this.newPass = passItem.totalNewPass;
        this.repeatPass = passItem.totalRepeatPass;
        this.totalPassPercent = passItem.totalNewPass + passItem.totalRepeatPass;
      });

      if (this.totalPassPercent > 0) {
        this.newPassPercent = Math.round((this.newPass / this.totalPassPercent) * 100);
        this.repeatPassPercent = Math.round((this.repeatPass / this.totalPassPercent) * 100);
      }

      this.options.series[0].data[0].value = this.repeatPassPercent;
      this.options.series[0].data[1].value = this.newPassPercent;

      this.options.series[1].data[0].value = this.repeatPassPercent;
      this.options.series[1].data[1].value = this.newPassPercent;

      this.echartsIntance.setOption(this.options);
    };
  }

  onChartInit(echarts) {
    this.echartsIntance = echarts;
  }

}
