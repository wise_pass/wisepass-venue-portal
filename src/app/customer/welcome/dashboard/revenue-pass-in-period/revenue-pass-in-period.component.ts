import { Component, OnInit } from '@angular/core';
import { UserDashboardService } from '../userdashboard.service';
import { ResponseData, ConsumersAndPassByBusinessList, ConsumersAndPassByBusinessModel } from '../userdashboard.model';

@Component({
  selector: 'ngx-revenue-pass-in-period',
  styleUrls: ['./revenue-pass-in-period.component.scss'],
  templateUrl: './revenue-pass-in-period.component.html',
})
export class RevenuePassInPeriodComponent implements OnInit {

  echartsIntance: any;
  echartsIntanceProfit: any;
  options: any;
  profitChart: any;
  totalNewConsumers: number = 0;
  totalRepeatConsumers: number = 0;
  totalNewPass: number = 0;
  totalRepeatPass: number = 0;
  currentMonth: string = "";
  businesss: ConsumersAndPassByBusinessModel[] = [];
  totalConsumersAndPassCurrentMonths = [];

  colors = [
    {
      lineColor1: '#7659ff',
      lineColor2: '#8357ff',
      areaColor1: 'rgba(78, 64, 164, 1)',
      areaColor2: 'rgba(118, 73, 208, 0.7)',
    },
    {
      lineColor1: '#00bece',
      lineColor2: '#00da78',
      areaColor1: 'rgba(38, 139, 145, 0.8)',
      areaColor2: 'rgba(38, 139, 145, 0.5)',
    },
    {
      lineColor1: '#8069ff',
      lineColor2: '#8357ff',
      areaColor1: 'rgba(118, 73, 208, 0.7)',
      areaColor2: 'rgba(188, 92, 255, 0.4)',
    }
  ];

  constructor(
    private _userDashboardService: UserDashboardService,
  ) { }

  ngOnInit() {
    this.getConsumersAndPass();
    this.getOptions();
    this.getProfitChart();
  }

  getConsumersAndPass() {
    this._userDashboardService.getConsumersAndPass()
      .subscribe(
        data => {
          try {
            const reponse = data as ResponseData<ConsumersAndPassByBusinessList>;
            this.businesss = reponse.data.consumersAndPassByBusinessList;
            console.log("call API revenue pass in period", this.businesss);

            var now = new Date();
            const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
              'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
            ];

            this.currentMonth = monthNames[now.getMonth()];
            if (this.businesss && this.businesss.length > 0) {
              this.businesss.forEach(businessItem => {
                const totalConsumersAndPassCurrentMonths = businessItem.totalConsumersAndPassList.find(x => x.month == this.currentMonth);
                if (totalConsumersAndPassCurrentMonths) {
                  this.totalNewPass = totalConsumersAndPassCurrentMonths.totalNewPass;
                  this.totalRepeatPass = totalConsumersAndPassCurrentMonths.totalRepeatPass;
                  this.totalNewConsumers = totalConsumersAndPassCurrentMonths.totalNewConsumers;
                  this.totalRepeatConsumers = totalConsumersAndPassCurrentMonths.totalRepeatConsumers;
                }
              });
              this.onDrawingChartCosumers();
              this.onDrawingChartPass();
            }
          } catch {
            // this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
        }, );
  }

  getOptions() {
    this.options = {
      backgroundColor: "#3d3780",
      grid: {
        left: "1%",
        right: "2%",
        top: "4%",
        bottom: "2%",
        containLabel: true
      },
      tooltip: {
        trigger: "axis",
        show: true,
        axisPointer: {
          type: "line",
          lineStyle: {
            color: "rgba(255, 255, 255, 0.1)",
            width: "1"
          }
        },
        textStyle: {
          color: "#ffffff",
          fontSize: "20",
          fontWeight: "normal"
        },
        position: "top",
        backgroundColor: "rgba(0, 255, 170, 0.35)",
        borderColor: "#00d977",
        borderWidth: 3,
        extraCssText: "box-shadow: 0px 2px 46px 0 rgba(0, 255, 170, 0.35); border-radius: 10px; padding: 8px 24px;"
      },
      xAxis: {
        type: "category",
        boundaryGap: false,
        offset: 5,
        data: [],
        axisTick: {
          show: false
        },
        axisLabel: {
          color: "#a1a1e5",
          fontSize: "14"
        },
        axisLine: {
          lineStyle: {
            color: "rgba(161, 161 ,229, 0.3)",
            width: "2"
          }
        }
      },
      legend: {
        x: 'left',
        y: 'top',
        textStyle: {
          color: '#fff',
        },
        data: [],
      },
      yAxis: {
        type: "value",
        boundaryGap: false,
        axisLine: {
          lineStyle: {
            color: "rgba(161, 161 ,229, 0.3)",
            width: "1"
          }
        },
        axisLabel: {
          color: "#a1a1e5",
          fontSize: "14"
        },
        axisTick: {
          show: false
        },
        splitLine: {
          lineStyle: {
            color: "rgba(161, 161 ,229, 0.2)",
            width: "1"
          }
        }
      },
      series: []
    };
  }

  getProfitChart() {
    this.profitChart = {
      "backgroundColor": "#3d3780",
      "tooltip": {
        "trigger": "axis",
        "axisPointer": {
          "type": "shadow",
          "shadowStyle": {
            "color": "rgba(0, 0, 0, 0.3)"
          }
        },
        textStyle: {
          color: "#ffffff",
          fontSize: "20",
          fontWeight: "normal"
        },
        position: "top",
        backgroundColor: "rgba(0, 255, 170, 0.35)",
        borderColor: "#00d977",
        borderWidth: 3,
        extraCssText: "box-shadow: 0px 2px 46px 0 rgba(0, 255, 170, 0.35); border-radius: 10px; padding: 8px 24px;"
      },
      grid: {
        left: "1%",
        right: "1%",
        top: "4%",
        bottom: "2%",
        containLabel: true
      },
      "legend": {
        x: 'left',
        y: 'top',
        "data": [],
        textStyle: {
          color: '#fff'
        }
      },
      "xAxis": [
        {
          "type": "category",
          "data": [],
          "axisTick": {
            "alignWithLabel": true
          },
          "axisLine": {
            "lineStyle": {
              "color": "#a1a1e5"
            }
          },
          "axisLabel": {
            "color": "#a1a1e5",
            "fontSize": "14"
          }
        }
      ],
      yAxis: {
        type: "value",
        boundaryGap: false,
        axisLine: {
          lineStyle: {
            color: "rgba(161, 161 ,229, 0.3)",
            width: "1"
          }
        },
        axisLabel: {
          color: "#a1a1e5",
          fontSize: "14"
        },
        axisTick: {
          show: false
        },
        splitLine: {
          lineStyle: {
            color: "rgba(161, 161 ,229, 0.2)",
            width: "1"
          }
        }
      },
      "series": []
    }
  }

  onDrawingChartCosumers() {
    let arrXaxisPass = [];
    let arrNewComsumers = [];
    let arrRepeatConsumers = [];

    this.businesss.forEach(businessItem => {
      businessItem.totalConsumersAndPassList.forEach(detailItem => {
        arrXaxisPass.push(detailItem.month);
        arrNewComsumers.push(detailItem.totalNewConsumers);
        arrRepeatConsumers.push(detailItem.totalRepeatConsumers);
      });
    });

    this.options.series = [];
    this.options.xAxis.data = arrXaxisPass;

    const series1 = this.getSeriesConsumers(this.colors[0].lineColor1, this.colors[0].areaColor1,
      this.colors[0].lineColor2, this.colors[0].areaColor2, 'New Consumers', arrNewComsumers);
    this.options.series.push(series1);

    const series2 = this.getSeriesConsumers(this.colors[1].lineColor1, this.colors[1].areaColor1,
      this.colors[1].lineColor2, this.colors[1].areaColor2, 'Repeat Consumers', arrRepeatConsumers);
    this.options.series.push(series2);

    this.echartsIntance.setOption(this.options);
  }

  onDrawingChartPass() {
    let arrXaxisPass = [];
    let arrNewPass = [];
    let arrRepeatPass = [];

    this.businesss.forEach(businessItem => {
      businessItem.totalConsumersAndPassList.forEach(detailItem => {
        arrXaxisPass.push(detailItem.month);
        arrNewPass.push(detailItem.totalNewPass);
        arrRepeatPass.push(detailItem.totalRepeatPass);
      });
    });

    this.profitChart.xAxis[0].data = arrXaxisPass;
    this.profitChart.series = [];

    const series1 = this.getSeriesPass(this.colors[0].lineColor1, this.colors[0].lineColor2, 'New PASS', arrNewPass);
    this.profitChart.series.push(series1);

    const series2 = this.getSeriesPass(this.colors[1].lineColor1, this.colors[1].lineColor2, 'Repeat PASS', arrRepeatPass);
    this.profitChart.series.push(series2);

    this.echartsIntanceProfit.setOption(this.profitChart);
  }

  private getSeriesConsumers(lineColor1, areaColor1, lineColor2, areaColor2, name, data) {
    return {
      "name": name,
      "type": "line",
      "smooth": true,
      "symbolSize": 20,
      "itemStyle": {
        "normal": {
          "opacity": 0,
          "color": {
            "x": 0,
            "y": 0,
            "x2": 0,
            "y2": 1,
            "type": "linear",
            "global": false,
            "colorStops": [
              {
                "offset": 0,
                "color": lineColor1
              },
              {
                "offset": 1,
                "color": lineColor2
              }
            ]
          }
        },
        "emphasis": {
          "opacity": 0
        }
      },
      lineStyle: {
        normal: {
          "width": "4",
          "type": "solid",
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: lineColor1,
          }, {
            offset: 1,
            color: lineColor2,
          }]),
        },
      },
      areaStyle: {
        normal: {
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: areaColor1,
          }, {
            offset: 1,
            color: areaColor2,
          }]),
        },
      },
      "data": data
    }
  }

  private getSeriesPass(colorStops1, colorStops2, name, data) {
    return {
      "name": name,
      "type": "bar",
      "barGap": 0,
      "barWidth": "20%",
      "itemStyle": {
        "normal": {
          "color": {
            "x": 0,
            "y": 0,
            "x2": 0,
            "y2": 1,
            "type": "linear",
            "global": false,
            "colorStops": [
              {
                "offset": 0,
                "color": colorStops1
              },
              {
                "offset": 1,
                "color": colorStops2
              }
            ]
          }
        }
      },
      "data": data
    }
  }

  onChartInit(echarts) {
    this.echartsIntance = echarts;
  }

  onChartProInit(echartsProfit) {
    this.echartsIntanceProfit = echartsProfit;
  }
}


