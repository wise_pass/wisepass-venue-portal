import { Component, OnInit } from '@angular/core';
import { ManagementVenuesService } from './management-venues.service';
import { ResponseData, ManagementVenuesModel, ManagementVenuesByBusinessModel, ManagementVenuesByVenueModel } from './management-venues.model';
import { Router } from '../../../../../node_modules/@angular/router';
import { ExcelService } from '../promotion-report/excel.service';



@Component({
  selector: 'ngx-management-venues',
  styleUrls: ['./management-venues.component.scss'],
  templateUrl: './management-venues.component.html',
})
export class ManagementVenuesComponent implements OnInit {

  loading: boolean;
  listManagementBusiness: ManagementVenuesByBusinessModel[];
  listManagementVenues: ManagementVenuesByVenueModel[];
  totalVenue = 0;
  totalVenueActive = 0;
  totalVenueDeactive = 0;

  constructor(
    private _managementVenuesService: ManagementVenuesService,
    private _router: Router,
    private excelService: ExcelService,
  ) { }

  settings = {
    columns: {
      numericalOrder: {
        title: 'STT',
        width: '5%'
      },
      businessVenueName: {
        title: 'Venue'
      },
      businessVenueTypeName: {
        title: 'Category',
        width: '30%',
      },
      city: {
        title: 'City',
        width: '30%',
      },
      isActive: {
        title: 'Status',
        type: 'html',
        width: '6%',
        valuePrepareFunction: (value) => {
          return value === true ? '<span class="btn-success">ON</span>' : '<span class="btn-danger">OFF</span>';
        }, 
      },
    },
    actions: false,
    pager: {
      display: true,
      perPage: 20
    },
  };

  ngOnInit() {
    this.onAPIManagementVenues();
  }

  onAPIManagementVenues() {
    this.loading = true;
    this._managementVenuesService.getManagementVenues()
      .subscribe(
        data => {
          try {
            const response = data as ResponseData<ManagementVenuesModel>;
            this.listManagementBusiness = response.data.managementVenuesByBusinessList;
            console.log('Call API Get Management Venues', this.listManagementBusiness);
            let turnCountActive = 1;
            if (this.listManagementBusiness && this.listManagementBusiness.length > 0) {
              this.listManagementBusiness.forEach(businessItem => {
                this.totalVenue = businessItem.totalVenue;
                this.listManagementVenues = businessItem.managementVenuesByVenueList;
                for (let index = 0; index < this.listManagementVenues.length; index++) {
                  console.log("TCL: ManagementVenuesComponent -> onAPIManagementVenues -> this.listManagementVenues.length", this.listManagementVenues.length)
                  const venueItem = this.listManagementVenues[index];
                  
                  if (venueItem.isActive == false) {
                    this.totalVenueDeactive = turnCountActive++;
                  }
                  this.totalVenueActive = this.totalVenue - this.totalVenueDeactive;
                }
              });
            }
          } catch (error) {
            console.log('errors', error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );
  }

  onExportReport() {
    let result = [];

    (this.listManagementBusiness || []).forEach(businessItem => {
      (businessItem.managementVenuesByVenueList || []).forEach(businessItem => {
        result.push({
          "Venue": businessItem.businessVenueName,
          "Category": businessItem.businessVenueTypeName,
          "City": businessItem.city,
          "Status": businessItem.isActive
        });
      });
    });
    this.excelService.exportAsExcelFile(result, "singha");
  }

}

