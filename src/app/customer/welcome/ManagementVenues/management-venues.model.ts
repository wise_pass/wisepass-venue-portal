
export interface ResponseData<T> {
    message: string;
    statusCode: number;
    data: T;
}
export interface ManagementVenuesModel {
    managementVenuesByBusinessList: Array<ManagementVenuesByBusinessModel>
}

export interface ManagementVenuesByBusinessModel {
    businessId: string;
    businessName: string;
    totalVenue: number;
    managementVenuesByVenueList: ManagementVenuesByVenueModel[];
}

export interface ManagementVenuesByVenueModel {
    businessVenueId: string;
    businessVenueName: string;
    businessVenueTypeName: string;
    fullAddress: string;
    city: string;
    isActive: boolean;
    numericalOrder: number;
}