
export interface PromotionCodeModel {
    message: string;
    statusCode: number;
    userPromoCampaign: UserPromoCampaign;
}

export interface UserPromoCampaign {
    campaigns: Campaigns[];
}

export interface Campaigns {
    id: string,
    name: string,
    isActive: boolean,
    promoCodes: PromoCodes[];
}

export interface PromoCodes {
    id: string,
    code: string,
    total: number,
    used: number,
    getReward: number,
    from: string,
    to: string,
    credits: Credits[];
    isShowCredit: boolean;
}

export interface Credits {
    id: string,
    createdDate: string,
    isUsed: boolean,
    usedTime: string,
    deviceType: string,
    deviceName: string,
    latitude: number,
    longitude: number,
    countryCode: string,
    udid: string,
    memberId: string,
    email: string,
    avatar: string,
    venueId: string,
    venueName: string,
}
