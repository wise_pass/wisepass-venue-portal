import { Component, OnInit, Injectable } from '@angular/core';
import { UserPromotionReportService } from './userPromotionReport.service';
import { PromotionCodeModel, Campaigns, PromoCodes } from './promotion-report.model';
import { ExcelService } from './excel.service';
import { Router } from '../../../../../node_modules/@angular/router';

@Injectable({
    providedIn: 'root'
})
@Component({
    selector: 'ngx-promotion-report',
    styleUrls: ['./promotion-report.component.scss'],
    templateUrl: './promotion-report.component.html',
})

export class PromotionReportComponent implements OnInit {

    promotionCodeArr: PromoCodes[] = [];
    promotionCodeFilter: PromoCodes[] = [];
    selectedDropDownItem: number = 0;
    isActive: boolean = true;
    seachKeyWord: string = '';
    checked = false;
    promotionCodeModel: PromotionCodeModel;
    arrCampaigns: Campaigns[];
    isDashboardShown = true;
    htmlStr: string;
    totalGetReward: number = 0;
    totalUsed: number = 0;
    arrCampaignsSelectItem: any = [];
    loading: boolean;

    constructor(
        private _userPromotionReportService: UserPromotionReportService,
        private excelService: ExcelService,
        private _router: Router,
    ) { }


    toggle(checked: boolean) {
        this.checked = checked;
    }

    ngOnInit() {
        this.loading = true;
        this._userPromotionReportService.getUserPromotionReport()
            .subscribe(
                data => {
                    try {
                        console.log('Call API Get User Dashboard');
                        this.promotionCodeModel = data as PromotionCodeModel;
                        this.arrCampaigns = this.promotionCodeModel.userPromoCampaign.campaigns;
                        // console.log(this.arrCampaigns);
                        if (this.arrCampaigns) {
                            this.onClickDropDownItem();
                        }
                    } catch (error) {
                        console.log("errors", error);
                        this._router.navigate(['/logout']);
                    }
                }, error => {
                    console.log('oops', error)
                },
                () => {
                    // 'onCompleted' callback.
                    // No errors, route to new page here
                    this.loading = false;
                },
        );


    }

    keyUpSearch() {
        this.promotionCodeFilter = this.promotionCodeArr.filter((itemProCode) => {
            return itemProCode.code.includes(this.seachKeyWord);
        });
    }

    onCheckBoxChange($event) {
    }

    onClickDropDownItem() {
        if (this.arrCampaigns) {
            this.promotionCodeFilter = this.promotionCodeArr = this.arrCampaigns[this.selectedDropDownItem].promoCodes;

            this.totalGetReward = 0;
            this.totalUsed = 0;
            for (let index = 0; index < this.promotionCodeFilter.length; index++) {
                const getReward = this.promotionCodeFilter[index].getReward;
                const used = this.promotionCodeFilter[index].used;

                if (getReward != 0) {
                    this.totalGetReward ++;
                }
                if (used != 0) {
                    this.totalUsed ++;
                }
            }
        }
    }

    onExportReport() {
        let result = [];

        if (this.arrCampaigns) {
            this.arrCampaignsSelectItem = this.arrCampaigns[this.selectedDropDownItem];
            console.log(this.arrCampaignsSelectItem);
            (this.arrCampaignsSelectItem.promoCodes || []).forEach(promoCodeItem => {
                if (promoCodeItem.credits.length > 0) {
                    (promoCodeItem.credits || []).forEach(creditsItem => {
                        result.push({
                            "Code Type": this.arrCampaignsSelectItem.name,
                            "Code Name": promoCodeItem.code,
                            "Activated Reward": promoCodeItem.getReward,
                            "Total Reward": promoCodeItem.total,
                            "Used Reward": promoCodeItem.used,
                            "Activation Date": promoCodeItem.from,
                            "Expiry Date": promoCodeItem.to,
                            "Email": creditsItem.email,
                            "Created Date": creditsItem.createdDate,
                            "Is Used?": creditsItem.isUsed,
                            "Used Time": creditsItem.usedTime,
                            "Venue": creditsItem.venueName
                        });
                    });
                } else {
                    result.push({
                        "Code Type": this.arrCampaignsSelectItem.name,
                        "Code Name": promoCodeItem.code,
                        "Activated Reward": promoCodeItem.getReward,
                        "Total Reward": promoCodeItem.total,
                        "Used Reward": promoCodeItem.used,
                        "Activation Date": promoCodeItem.from,
                        "Expiry Date": promoCodeItem.to,
                        "Email": "",
                        "Created Date": "",
                        "Is Used?": "",
                        "Used Time": "",
                        "Venue": ""
                    });
                }
            });

            console.log(result);
            this.excelService.exportAsExcelFile(result, this.arrCampaignsSelectItem.name);
        }
    }

}

