import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { WelcomeRoutingModule, routedComponents } from './welcome-routing.module';
import { ButtonsModule } from './buttons/buttons.module';

// Services
import { CheckTokenService } from '../../configuration/check-token-service';
import { InformationService } from './information/information.service';
import { UserDashboardService } from './dashboard/userdashboard.service';
import { NbCardModule } from '../../../../node_modules/@nebular/theme';
import { NgxEchartsModule } from '../../../../node_modules/ngx-echarts';
import { UserPromotionReportService } from './promotion-report/userPromotionReport.service';
import { ExcelService } from './promotion-report/excel.service';
import { AdminPermissionService } from './admin-permissions/admin-permission.service';
import { UniqueMembershipReportService } from './unique-membership-report/unique-membership-report.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PassBreakDownService } from './pass-breakdown/pass-breakdown.service';
import { PushNotificationService } from './pushNotification/pushNotification.service';
import { EventService } from './events/event.service';
import { ManagementVenuesService } from './ManagementVenues/management-venues.service';

@NgModule({
  imports: [
    ThemeModule,
    WelcomeRoutingModule,
    ButtonsModule,
    NbCardModule,
    NgxEchartsModule,
    // DpDatePickerModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    CheckTokenService,
    InformationService,
    UserDashboardService,
    UserPromotionReportService,
    ExcelService,
    AdminPermissionService,
    UniqueMembershipReportService,
    PassBreakDownService,
    EventService,
    PushNotificationService,
    ManagementVenuesService,
  ]
})
export class WelcomeModule { }
