export interface DashboardModel {
  data: Data;
  message: string;
  statusCode: number;
}

export interface Data {
  business: Business[];
}

export interface Business {
  isBusiness: boolean;
  venues: Venue[];
  id: string;
  name: string;
  currency: string;
  info: Info;
  analytics: Analytics;
  activity: Activity[];
}

export interface Venue {
  isBusiness: boolean;
  id: string;
  name: string;
  currency: string;
  info: Info;
  analytics: Analytics;
  activity: Activity[];
}

export interface Analytics {
  totalPass: number;
  totalNewPass: number;
  totalRepeatPass: number;
  details: DetailItemInPass[];
}

export interface DetailItemInPass {
  key: string;
  totalPass: number;
  totalUniquePass: number;
  totalNewPass: number;
  totalRepeatPass: number;
}

export interface Info {
  totalPass: number;
  totalRevenue: number;
  totalPassDay: number;
  totalRevenueDay: number;
  year: Period;
  month: Period;
  week: Period;
}
export interface Period {
  totalPass: number;
  totalRevenue: number;
  details: DetailItemInPeriod[];
}

export interface DetailItemInPeriod {
  key: string;
  totalPass: number;
  totalRevenue: number;
  percentPassWithPrevious: number;
  keyPrevious: number;
  values: Values[];
}

export interface Values {
  productType: string;
  numberOfScans: number;
  price: number;
}

export interface Activity {
  productType: string;
  createdDateText: string;
  createdTimeText: string;
  price: number;
}

// PASS Analytic
export interface PassBusinessModel {
  data: Data;
  message: string;
  statusCode: number;
}
export interface Data {
  businessPass: BusinessPass[];
}

export interface BusinessPass {
  isBusiness: boolean;
  id: string;
  businessName: string;
  venuesPass: VenuesPass[];
  venueByPeriod: VenueByPeriod[];
}

export interface VenuesPass {
  isBusiness: boolean;
  id: string;
  venueName: string;
  venueByPeriod: VenueByPeriod[];

}

export interface VenueByPeriod {
  month: string;
  totalNewPass: number;
  totalPass: number;
  totalRepeatPass: number;
  totalUniquePass: number;
}

