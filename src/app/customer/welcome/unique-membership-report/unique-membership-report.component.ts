import { Component, OnInit, Injectable } from '@angular/core';
import { UniqueMembershipReportService } from './unique-membership-report.service';
import { ReponseDataList, UniqueMembershipBusinessModel, UniqueMembershipBusinessList } from './unique-membership-report.model';

@Injectable({
    providedIn: 'root'
})
@Component({
    selector: 'ngx-unique-membership-report',
    styleUrls: ['./unique-membership-report.component.scss'],
    templateUrl: './unique-membership-report.component.html',
})

export class UniqueMembershipReportComponent implements OnInit {

    uniqueMembershipBusiness: UniqueMembershipBusinessModel[] = [];
    constructor(
        private _uniqueMembershipReportService: UniqueMembershipReportService,
    ) { }

    ngOnInit() {
        this._uniqueMembershipReportService.getUniqueMembershipReport()
            .subscribe(
                data => {
                    try {
                        const reponse = data as ReponseDataList<UniqueMembershipBusinessList>;
                        this.uniqueMembershipBusiness = reponse.data.uniqueMembershipBusinessList;
                        console.log('Call API Get User Unique Membership', this.uniqueMembershipBusiness);
                    } catch (error) {
                        console.log("errors", error);
                        // this._router.navigate(['/logout']);
                    }
                }, error => {
                    console.log('oops', error)
                },
                () => {
                    // 'onCompleted' callback.
                    // No errors, route to new page here
                },
        );


    }

}

