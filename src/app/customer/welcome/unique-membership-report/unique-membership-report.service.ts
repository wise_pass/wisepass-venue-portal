import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/observable/of';
import { ConfigService } from '../../../configuration/configuration';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class UniqueMembershipReportService {

    constructor(private http: HttpClient, public _configService: ConfigService) {}

    httpOptions = {
        headers: {},
    };

    getDecodedAccessToken(token: string): any {
        try {
            return jwt_decode(token);
        }
        catch (Error) {
            return null;
        }
    }

    initHttpHeader() {
        this.httpOptions.headers = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Authorization': (localStorage.getItem('token') ? localStorage.getItem('token') : ''),
            },
        );
    }

    getUniqueMembershipReport() {
        this.initHttpHeader();
        return this.http.get(
            this._configService.getAPILink('uniquemembershipreport').toString(),
            this.httpOptions);
    }
}
