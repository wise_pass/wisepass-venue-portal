export interface ReponseDataList<T> {
    message: string;
    statusCode: number;
    data: T;
}

export interface UniqueMembershipBusinessList {
    uniqueMembershipBusinessList: Array<UniqueMembershipBusinessModel>;
}

export interface UniqueMembershipBusinessModel{
    businessId :  string;
    businessName: string;
    totalUniqueMembershipAll: number;
    uniqueMembershipBusinessVenueList : UniqueMembershipBusinessVenuesModel[]
}

export interface UniqueMembershipBusinessVenuesModel{
    businessVenueId :  string;
    businessVenueName: string;
    totalUniqueMembership: number;
}