import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeComponent } from './welcome.component';
import { WelcomeInformationComponent } from './information/information.component';

import { FormInputsComponent } from './form-inputs/form-inputs.component';
import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PassInPeriodComponent } from './dashboard/pass-in-period/pass-in-period.component';
import { RevenuePassInPeriodComponent } from './dashboard/revenue-pass-in-period/revenue-pass-in-period.component';
import { PassActivityComponent } from './dashboard/pass-activity/pass-activity.component';
import { PassAnalyticComponent } from './dashboard/pass-analytic/pass-analytic.component';
import { SlideOutComponent } from './dashboard/slide-out/slide-out.component';
import { NestedMenuComponent } from './dashboard/nested-menu/nested-menu.component';
import { PromotionReportComponent } from './promotion-report/promotion-report.component';
import { AdminPermissionComponent } from './admin-permissions/admin-permission.component';
import { UniqueMembershipReportComponent } from './unique-membership-report/unique-membership-report.component';
import { ConsumerComponent } from './dashboard/consumers/consumer.component';
import { PassComponent } from './dashboard/pass/pass.component'; 
import { PassBreakDownComponent } from './pass-breakdown/pass-breakdown.component';
import { PushNotificationComponent } from './pushNotification/pushNotification.component';
import { EventComponent } from './events/event.component';
import { ManagementVenuesComponent } from './ManagementVenues/management-venues.component';
const routes: Routes = [{
  path: '',
  component: WelcomeComponent,
  children: [
    {
      path: 'information',
      component: WelcomeInformationComponent,
    },
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      path: 'promotionreport',
      component: PromotionReportComponent,
    },
    {
      path: 'adminpermission',
      component: AdminPermissionComponent,
    },
    {
      path: 'uniquemembershipreport',
      component: UniqueMembershipReportComponent,
    },
    {
      path: 'passbreakdown',
      component: PassBreakDownComponent,
    },
    {
      path: 'managementvenues',
      component: ManagementVenuesComponent,
    },
    {
      path: 'event',
      component: EventComponent,
    },
    {
      path: 'pushnotification',
      component: PushNotificationComponent,
    },
    {
      path: 'inputs',
      component: FormInputsComponent,
    },
    {
      path: 'layouts',
      component: FormLayoutsComponent,
    },
    {
      path: 'layouts',
      component: FormLayoutsComponent,
    },
    {
      path: 'buttons',
      component: ButtonsComponent,
    },
    {
      path: 'datepicker',
      component: DatepickerComponent,
    }, {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    }
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class WelcomeRoutingModule {

}

export const routedComponents = [
  // Root 
  WelcomeComponent,

  // Child
  WelcomeInformationComponent,
  DashboardComponent,
  ConsumerComponent,
  PassComponent,
  PassInPeriodComponent,
  RevenuePassInPeriodComponent,
  PassActivityComponent,
  PassAnalyticComponent,
  SlideOutComponent,
  NestedMenuComponent,
  PromotionReportComponent,
  AdminPermissionComponent,
  UniqueMembershipReportComponent,
  PassBreakDownComponent,
  EventComponent,
  PushNotificationComponent,
  ManagementVenuesComponent,
  
  FormInputsComponent,
  FormLayoutsComponent,
  DatepickerComponent
];
