
export interface ReponseDataList<T> {
  message: string;
  statusCode: number;
  data: Array<T>;
}


export interface AdminPermissionModel {
  businessId: string;
  businessName: string;
  memberEmails: Email[];
}

export interface Email {
  id: string;
  memberEmail: string;
}