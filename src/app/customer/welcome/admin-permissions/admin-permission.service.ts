import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/observable/of';
import { ConfigService } from '../../../configuration/configuration';
import * as jwt_decode from 'jwt-decode';
import { AdminPermissionModel } from './admin-permission.model';

@Injectable()
export class AdminPermissionService {

    constructor(private http: HttpClient, public _configService: ConfigService) {}

    httpOptions = {
        headers: {},
    };

    getDecodedAccessToken(token: string): any {
        try {
            return jwt_decode(token);
        }
        catch (Error) {
            return null;
        }
    }

    initHttpHeader() {
        this.httpOptions.headers = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Authorization': (localStorage.getItem('token') ? localStorage.getItem('token') : ''),
            },
        );
    }

    getAdminPermissionReport(memberId) {
        this.initHttpHeader();
        var body = {
            "MemberId" : memberId
        }
        return this.http.post(
            this._configService.getAPILink('getadminpermissionreport').toString(),
            body,
            this.httpOptions);
    }

    updatMemberByBusiness(adminPermission: AdminPermissionModel) {
        this.initHttpHeader();
        return this.http.post(
            this._configService.getAPILink('updatmemberbybusiness').toString(),
            adminPermission,
            this.httpOptions);
    }
}
