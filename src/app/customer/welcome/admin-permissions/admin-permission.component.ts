import { Component, OnInit, Injectable } from '@angular/core';
import { AdminPermissionService } from './admin-permission.service';
import { ReponseDataList, AdminPermissionModel, Email } from './admin-permission.model';
import { NbToastrService } from '../../../../../node_modules/@nebular/theme/components/toastr/toastr.service';
import * as jwt_decode from 'jwt-decode';


@Injectable({
    providedIn: 'root'
})
@Component({
    selector: 'ngx-admin-permission',
    styleUrls: ['./admin-permission.component.scss'],
    templateUrl: './admin-permission.component.html',
})

export class AdminPermissionComponent implements OnInit {

    groupBusinessFilter: AdminPermissionModel[] = [];
    adminPermissions: AdminPermissionModel[] = [];
    seachKeyWord: string;
    memberId: string;
    email: Email[] = [];

    constructor(
        private _adminPermissionService: AdminPermissionService,
        private toastrService: NbToastrService,
    ) { }

    ngOnInit() {
        const infoJwt = this.getDecodedAccessToken(localStorage.getItem('token') ? localStorage.getItem('token') : '');
       
        if (infoJwt) {
           this.memberId =  infoJwt.UserId;
           this.getBottleConsumption();
        }else{
            this.toastrService.danger('Session Timeout');
        }
    }

    getDecodedAccessToken(token: string): any {
        try {
            return jwt_decode(token);
        }
        catch (Error){
            return null;
        }
      }

    getBottleConsumption() {
        this._adminPermissionService.getAdminPermissionReport(this.memberId)
            .subscribe(
                data => {
                    try {
                        const response = data as ReponseDataList<AdminPermissionModel>;
                        this.groupBusinessFilter = response.data;

                        // this.keyUpSearch();
                        console.log('Call API Get User Admin', this.groupBusinessFilter);

                    } catch {
                        // this._router.navigate(['/logout']);
                    }
                }, error => {
                    console.log('oops', error)
                },
                () => {
                    // 'onCompleted' callback.
                    // No errors, route to new page here
                },
        );
    }

    keyUpSearch() {
        // console.log(this.seachKeyWord);
        // if (this.groupBusinessFilter) {
        //     this.adminPermissions = this.groupBusinessFilter.filter((item) => {
        //         return (item.businessName || '').toLowerCase().includes(this.seachKeyWord.toLowerCase());
        //     });
        // }
    }

    onClickAddInput(adminPermission: AdminPermissionModel) {
        adminPermission.memberEmails.push(
            {
                id: null,
                memberEmail: ""
            }
        );
    }

    onClickDeleteInput(adminPermission: AdminPermissionModel, index : number){
        adminPermission.memberEmails.splice(index); 
        if (index != 0){
          adminPermission.memberEmails.splice(index); 
        }else{
          adminPermission.memberEmails[index].memberEmail = "";
        }
      }
      
    onClickSubmit(adminPermission: AdminPermissionModel, position, status) {
        console.log(adminPermission);
        this._adminPermissionService.updatMemberByBusiness(adminPermission)
            .subscribe(
                data => {
                    try {
                        console.log(data);
                        const response = data as ReponseDataList<AdminPermissionModel>;
                        console.log('Call API Get User Update', response);
                        if (response.statusCode == 200) {
                            this.toastrService.show(
                                status || 'Success',
                                adminPermission.businessName,
                                { position, status });
                        }
                    } catch (error) {
                        console.log("errors", error);
                        // this._router.navigate(['/logout']);
                    }
                }, error => {
                    console.log('oops', error)
                },
                () => {
                    // 'onCompleted' callback.
                    // No errors, route to new page here
                },
        );
    }

}

