
export interface ReponseDataList<T> {
    message: string;
    statusCode: number;
    data: T;
}

// Vendor
export interface EventModel {
    events: Array<EventsList>
}

export interface EventsList {
    id: string;
    name: string;
    businessVenue: string;
    createdDate: string;
    city: string;
    registrations: number;
    status: string;
    totalPass: number;
    numericalOrder: number;
    image: string;
}