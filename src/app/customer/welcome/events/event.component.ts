import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../../node_modules/@angular/router';
import { EventService } from './event.service';
import { ReponseDataList, EventModel, EventsList } from './event.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-event',
  styleUrls: ['./event.component.scss'],
  templateUrl: './event.component.html',
})
export class EventComponent implements OnInit {
  loading: boolean;
  events: EventsList[] = [];

  settings = {
    columns: {
      numericalOrder: {
        title: 'STT',
        width: '5%'
      },
      name: {
        title: 'Name',
        filter: true,
        sort: false,
      },
      businessVenue: {
        title: 'Venue',
        filter: true,
      },
      createdDate: {
        title: 'Date', sort: false,
        valuePrepareFunction: (data) => {
          var raw = new Date(data);
          var formatted = this.datePipe.transform(raw, 'dd MMM yyyy');
          return formatted;
        },
      },
      city: {
        title: 'City',
        sort: false
      },
      registrations: {
        title: 'Registrations', sort: false,
      },
      totalConsumers: {
        title: 'Consumer',
      },
      totalPass: {
        title: 'PASS',
      },
      status: {
        title: 'Status',
      },
      image: {
        title: 'Content',
        width: '5%',
        type: 'html',
        valuePrepareFunction: (data) => {
          if (data != null) {
            return '<a class="image-event-fb" href="' + data + '">Image</a>';
          } else {
            return null
          }
        },

      },
    },
    actions: {
      columnTitle: 'Actions',
      add: false,
      edit: false,
      delete: false,
    },
    pager: {
      display: true,
      perPage: 20
    },

  };

  constructor(
    private _eventService: EventService,
    private _router: Router,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.onAPIEvent();
  }

  onAPIEvent() {
    this.loading = true;
    // getvenue
    this._eventService.getEvents()
      .subscribe(
        data => {
          try {
            const response = data as ReponseDataList<EventModel>;
            this.events = response.data.events;
            console.log('Call API Get Event', this.events);

          } catch (error) {
            console.log("errors", error);
            this._router.navigate(['/logout']);
          }
        }, error => {
          console.log('oops', error)
        },
        () => {
          // 'onCompleted' callback.
          // No errors, route to new page here
          this.loading = false;
        },
    );
  }
}

