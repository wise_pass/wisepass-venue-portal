import { Component } from '@angular/core';

import { MENU_ITEMS } from './customer-menu';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['customer.component.scss'],
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent {
  menu = MENU_ITEMS;
}
