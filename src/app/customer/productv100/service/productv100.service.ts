import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/observable/of';
import { ConfigService } from '../../../configuration/configuration';
import * as jwt_decode from "jwt-decode";

@Injectable()
export class ProductV100Service {

    constructor(private http: HttpClient, public _configService: ConfigService) {}

    httpOptions = {
        headers: {},
    };

    getDecodedAccessToken(token: string): any {
        try {
            return jwt_decode(token);
        }
        catch (Error){
            return null;
        }
    }

    initHttpHeader() {
        this.httpOptions.headers = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Token': (localStorage.getItem('token') ? localStorage.getItem('token') : ''),
            },
        );
    }

    getgroup(item) {
        this.initHttpHeader();
        return this.http.post(
            this._configService.getAPILink('productgroup').toString() + 'list',
            item);
    }
}
