import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductV100Component } from './productv100.component';
// import { SheetComponent } from './sheet/sheet.component';

import { FormInputsComponent } from './form-inputs/form-inputs.component';
import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { ButtonsComponent } from './buttons/buttons.component';

const routes: Routes = [{
  path: '',
  component: ProductV100Component,
  children: [
    // {
    //   path: 'sheet',
    //   component: SheetComponent,
    // },
    {
      path: 'inputs',
      component: FormInputsComponent,
    },
    {
      path: 'layouts',
      component: FormLayoutsComponent,
    },
    {
      path: 'layouts',
      component: FormLayoutsComponent,
    },
    {
      path: 'buttons',
      component: ButtonsComponent,
    },
    {
      path: 'datepicker',
      component: DatepickerComponent,
    },
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class ProductV100RoutingModule {

}

export const routedComponents = [
  ProductV100Component,
  // SheetComponent,

  FormInputsComponent,
  FormLayoutsComponent,
  DatepickerComponent,
];
