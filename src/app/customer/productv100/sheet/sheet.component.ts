// import { Component, OnInit } from '@angular/core';
// import { LocalDataSource } from 'ng2-smart-table';
// import { SmartTableService } from '../../../@core/data/smart-table.service';

// @Component({
//   selector: 'ngx-sheet',
//   styleUrls: ['./sheet.component.scss'],
//   templateUrl: './sheet.component.html',
// })
// export class SheetComponent implements OnInit {
//   ngOnInit(): void {
     
//   }
//   model = {
//     language: 'vi',
//     title: 'Phiên bản 1.00'
//   }

//   settings = {
//     add: {
//       addButtonContent: '<i class="nb-plus"></i>',
//       createButtonContent: '<i class="nb-checkmark"></i>',
//       cancelButtonContent: '<i class="nb-close"></i>',
//     },
//     edit: {
//       editButtonContent: '<i class="nb-edit"></i>',
//       saveButtonContent: '<i class="nb-checkmark"></i>',
//       cancelButtonContent: '<i class="nb-close"></i>',
//     },
//     delete: {
//       deleteButtonContent: '<i class="nb-trash"></i>',
//       confirmDelete: true,
//     },
//     columns: {
//       id: {
//         title: 'ID',
//         type: 'number',
//       },
//       firstName: {
//         title: 'First Name',
//         type: 'string',
//       },
//       lastName: {
//         title: 'Last Name',
//         type: 'string',
//       },
//       username: {
//         title: 'Username',
//         type: 'string',
//       },
//       email: {
//         title: 'E-mail',
//         type: 'string',
//       },
//       age: {
//         title: 'Age',
//         type: 'number',
//       },
//     },
//   };
//   source: LocalDataSource = new LocalDataSource();
//   constructor(private service: SmartTableService,
//     const data = this.service.getData();
//     this.source.load(data);
//   }

//   onDeleteConfirm(event): void {
//     if (window.confirm('Are you sure you want to delete?')) {
//       event.confirm.resolve();
//     } else {
//       event.confirm.reject();
//     }
//   }

//   starRate = 2;
//   heartRate = 4;
//   radioGroupValue = 'This is value 2';

//   ngOnInit() {

//     this.model.language = localStorage.getItem('language') ? localStorage.getItem('language') : '';
//     if (this.model.language == 'en') {
//       this.model.title = 'This is Sheet Product version 1.00';
//     }

//     // this._productService.getgroup({Id: 0, Name: ''})
//     // .subscribe(
//     //   data => {

//     //   },
//     //   err => { 

//     //   },
//     //   () => { },
//     // );
//   }

// }