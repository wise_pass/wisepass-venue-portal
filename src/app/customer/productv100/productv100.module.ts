import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { ProductV100RoutingModule, routedComponents } from './productv100-routing.module';
import { ButtonsModule } from './buttons/buttons.module';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { SmartTableService } from '../../@core/data/smart-table.service';
import { ProductV100Service } from './service/productv100.service';
import { httpInterceptorProviders } from '../../http-interceptors/index';

@NgModule({
  imports: [
    ThemeModule,
    ProductV100RoutingModule,
    ButtonsModule,
    Ng2SmartTableModule
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    SmartTableService,
    ProductV100Service,
    httpInterceptorProviders
  ],
})
export class ProductV100Module { }
