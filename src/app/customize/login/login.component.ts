import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.scss'],
})
export class CustomizeLoginComponent implements OnInit {

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      firstName: {
        title: 'First Name',
        type: 'string',
      },
      lastName: {
        title: 'Last Name',
        type: 'string',
      },
      username: {
        title: 'Username',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
      age: {
        title: 'Age',
        type: 'number',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(
    private _loginService: LoginService,
    public router: Router,
  ) { }

  model = {
    isLogin: null,
    input: {
      UserName: '',
      Password: '',
    },
    textError: '',
    textSuccess: '',
    statusLogin: 0,
    inputValidateEmail: {
      Email: ''
    },
    inputValidateOtp: {
      Email: '',
      Otp: ''
    },
    OtpInput: {
      UmbrellaOne: '',
      UmbrellaTwo: '',
      UmbrellaThree: '',
      UmbrellaFour: ''
    }
  }



  getDecodedAccessToken(token: string): any {
    try {
        return jwt_decode(token);
    }
    catch (Error){
        return null;
    }
  }

  public onGoHome(event) {
    this.router.navigate(['/']);
  }

  public onLogout(event) {
    this.logout(localStorage.getItem('token'));
  }

  onKeydown(object, event) {
    if (event.key === 'Enter') {
      if (object === 'validateemail') {
        this.onValidateEmail();
      } else if (object == 'validateotp') {
        this.onValidateOtp();
      }
    }
  }

  onValidateEmail() {
    this._loginService.login(this.model.inputValidateEmail)
    .subscribe(
      data => {
        const jsonObject = data as LoginReturnModel;
        if (jsonObject.statusCode) {
          if (jsonObject.statusCode != 200) {
            this.model.textError = jsonObject.message;
          }
          else if (jsonObject.statusCode === 200)
          {
            this.model.inputValidateOtp.Email = this.model.inputValidateEmail.Email;
            this.model.textError = '';
            this.model.statusLogin = 1;
          }
        }
      },
      err => { },
      () => { },
    );
  }

  next(e, item) {
    // console.log(e, item);
    if (e.keyCode >= 48 && e.keyCode <= 57 || e.keyCode >= 96 && e.keyCode <= 105) {
      item.focus();
      if (this.model.OtpInput.UmbrellaOne
        && this.model.OtpInput.UmbrellaTwo
        && this.model.OtpInput.UmbrellaThree
        && this.model.OtpInput.UmbrellaFour) {
        this.model.inputValidateOtp.Otp = this.model.OtpInput.UmbrellaOne + this.model.OtpInput.UmbrellaTwo + this.model.OtpInput.UmbrellaThree + this.model.OtpInput.UmbrellaFour;
        this.onValidateOtp();
        // console.log(this.model.inputValidateOtp.Otp);
      }
    } else {
    }
  }

  onValidateOtp() {
    this._loginService.validateOtp(this.model.inputValidateOtp)
    .subscribe(
      data => {
        const jsonObject = data as ValidateOtpReturnModel;
        if (jsonObject)
        {
          if (jsonObject.data && jsonObject.data.account && jsonObject.statusCode === 200)
          {
            if (jsonObject.data.account.token)
            {
              this.model.textError = '';
              this.model.textSuccess = 'Redirect to dashboard, please wait 3 seconds ...';
              localStorage.setItem('token', jsonObject.data.account.token);
              this.router.navigate(['/customer/welcome'])
            }
          } else {
            if (jsonObject.statusCode)
            {
              this.model.textError = jsonObject.message;
            }
          }
        }
      },
      err => { },
      () => { },
    );
  }

  logout(item) {
    this.router.navigate(['/logout']);
  }

  checkCurrentLogin() {
    if (localStorage.getItem('token') &&
        localStorage.getItem('token').length > 0) {
      this.model.isLogin = true;
    } else {
      this.model.isLogin = false;
    }
  }

  ngOnInit() {
    this.checkCurrentLogin();
  }

}

interface BaseReturnAPI {
  value: {
    code: number,
    type: string,
    status: string,
    content: string,
  }
}

interface LoginReturnModel {
  data: any;
  statusCode: number;
  message: string,
}

interface ValidateOtpReturnModel {
  data: {
    account: {
      avatarUrl: string;
      name: string;
      token: string
    }
  }
  statusCode: number;
  message: string
}
