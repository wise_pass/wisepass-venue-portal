import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import * as Rx from 'rxjs/Rx';

@Injectable()
export class StorageService {
    private storageSub = new Rx.Subject<string>();

    watchStorage(): Observable<any> {
        return this.storageSub.asObservable();
    }

    setItem(key: string, data: any) {
        localStorage.setItem(key, data);
        this.storageSub.next('changed');
    }

    removeItem(key) {
        localStorage.removeItem(key);
        this.storageSub.next('changed');
    }
}
