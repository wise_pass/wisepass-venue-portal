import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class ConfigService {
  private apiLink = {
      main: environment.main,
  }

  private listAPI = [
    {name: 'validateemail', link: 'portal/auth/validateemail'},
    {name: 'validateotp', link: 'portal/auth/validateotp'},
    {name: 'getvenueinformation', link: 'portal/venue/information'},
    {name: 'savevenueinformation', link: 'portal/venue/v2/information'},
    {name: 'promotionReportInformation', link: 'portal/user/promocampaign'},
    {name: 'getadminpermissionreport', link: 'portal/venue/adminpermission'},
    {name: 'updatmemberbybusiness', link: 'portal/venue/updatmemberbybusiness'},
    {name: 'uniquemembershipreport', link: 'portal/venue/uniquemembershipreport'},
    {name: 'passbreakdown', link: 'portal/venuemonthuserconsumptionreport'},
    // Supplier Consumption 
    {name: 'passsupplierconsumers', link: 'portal/user/passsupplierconsumers'},
    {name: 'getconsumersandpass', link: 'portal/user/getconsumersandpass'},

    { name: 'pushNotification', link: 'portal/venue/pushnotications' },
    { name: 'getPushNotification', link: 'portal/venue/supplierpushnotification' },

    { name: 'event', link: 'portal/venue/events' },
    { name: 'managementvenues', link: 'portalAdmin/managementvenues' }, // Only Singha

     // Dashboard
     { name: 'dashboard', link: 'portalAdmin/dashboard' },
  ];

  constructor() {
  }

  getAPILink(key) {
    var item = this.listAPI.filter(function(item) {
      return item.name === key;
    })[0];

    return this.apiLink.main + item.link;
  }
}
