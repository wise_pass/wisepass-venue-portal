import { Injectable  } from '@angular/core';

import 'rxjs/add/operator/do';
// import { AuthService } from './auth.service';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { HttpResponse } from '@angular/common/http/';
import { LoginService } from '../customize/login/login.service';
import { Router } from '@angular/router';

import { ToasterConfig } from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import { NbToastStatus } from '@nebular/theme/components/toastr/model';
import { NbGlobalPhysicalPosition, NbGlobalPosition, NbToastrService } from '@nebular/theme';
import * as jwt_decode from 'jwt-decode';
import { Observable } from 'rxjs';


interface LoginReturnModel {
  obj: string;
  status: number;
}

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
  returnUrl: string;
  constructor(
    private _loginService: LoginService,
    public router: Router,
    private toastrService: NbToastrService,
  ) {
    // this.returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'] || '/';
  }

  config: ToasterConfig;
  index = 1;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  status: NbToastStatus = NbToastStatus.SUCCESS;

  private showToast(type: NbToastStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 5000,
      hasIcon: true,
      position: this.position,
      preventDuplicates: false,
    };
    const titleContent = title ? `${title}` : '';

    this.index += 1;
    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const authToken = localStorage.getItem('token') ? localStorage.getItem('token') : '';
    const authReq = request.clone({ setHeaders: { Token: authToken } });

    return next.handle(authReq).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // do stuff with response if you want
        if (event.body.code === 417) {
          return this._loginService.refresh({ Token: (localStorage.getItem('token') ? localStorage.getItem('token') : '') })
            .subscribe(
              data => {
                const jsonObject = data as LoginReturnModel;
                if (jsonObject.obj && jsonObject.status == 200) {
                  localStorage.setItem('token', jsonObject.obj);
                  var jwt = this.getDecodedAccessToken(jsonObject.obj);
                  localStorage.setItem('language', jwt.language ? jwt.language : "");

                  this.showToast(this.status, 'Refresh Token Succeed', 'Hope you retry function, thank you !');

                  var jwt = this.getDecodedAccessToken(jsonObject.obj);
                  localStorage.setItem('language', jwt.language ? jwt.language : "");
                }
                else if (jsonObject.obj && jsonObject.status == 100) {
                  localStorage.removeItem('token');
                  this.router.navigate(['/login']);
                }
              },
              err => {
                localStorage.removeItem('token');
                this.router.navigate(['/login']);
              },
              () => {
                const customReq = request.clone({
                  headers: request.headers.set('token', localStorage.getItem('token') ? localStorage.getItem('token') : '')
                });
                return next.handle(customReq);
              },
          )


          // return this._loginService.refresh({Token: (localStorage.getItem('token') ? localStorage.getItem('token') : '')})
          // .subscribe(
          //   data => {
          //     const jsonObject = data as LoginReturnModel;
          //     if (jsonObject.obj && jsonObject.status == 200) {
          //       localStorage.setItem('token', jsonObject.obj);
          //       var jwt = this.getDecodedAccessToken(jsonObject.obj);
          //       localStorage.setItem('language', jwt.language ? jwt.language : "");

          //       const customReq = request.clone({
          //         headers: request.headers.set('token', jsonObject.obj)
          //       });

          //       this.showToast(this.status, 'Refresh Token', 'Update login status succeed !');

          //       var jwt = this.getDecodedAccessToken(jsonObject.obj);
          //       localStorage.setItem('language', jwt.language ? jwt.language : "");

          //       return next.handle(customReq);
          //     }
          //     else if (jsonObject.obj && jsonObject.status == 100)
          //     {
          //       localStorage.removeItem('token');
          //       this.router.navigate(['/login']);
          //     }
          //   },
          //   err => {
          //     localStorage.removeItem('token');
          //     this.router.navigate(['/login']);
          //   },
          //   () => { },
          // )
        }
      }
    })
  }
}