﻿// Youtube
fetch('https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.youtube.com%2Ffeeds%2Fvideos.xml%3Fplaylist_id%3DPLf9ts8zo7MFsWiM8klMBdrBa7BQY2EWt-')
    .then((res) => res.json())
    .then((data) => {
        const res = data.items

        function toText(node) {
            let tag = document.createElement('div')
            tag.innerHTML = node
            node = tag.innerText
            return node
        }
        function shortenText(text, startingPoint, maxLength) {
            return text.length > maxLength ?
                text.slice(startingPoint, maxLength) :
                text
        }
        var resSlice = res.slice(0, 10);

        //youtube
        let videoBody = '';
        resSlice.forEach((item) => {
            var url = item.link;
            //var res = url.replace("watch?v=", "embed/");

            videoBody += `<div class="rvs-item" style="background-image: url(${item.thumbnail})">
                    <p class="rvs-item-text">${item.title}<small>by WisePass </small></p>
                    <a href="${url}" class="rvs-play-video"></a>
                </div>
                         `
        })
        document.querySelector('.rvs-item-stage').innerHTML = videoBody;

        //youtube
        let videoBodyDetail = '';
        resSlice.forEach((item) => {
            var url = item.link;
            var res = url.replace("watch?v=", "embed/");

            videoBodyDetail += `<a class="rvs-nav-item">
                        <span class="rvs-nav-item-thumb" style="background-image: url(${item.thumbnail})"></span>
                        <h4 class="rvs-nav-item-title">${item.title}</h4>
                        <small class="rvs-nav-item-credits">by WisePass</small>
                    </a>
                         `
        })
        document.querySelector('.rvs-nav-stage').innerHTML = videoBodyDetail;
        $(document).ready(function () {
            $(".rvs-container").rvslider();
        });
    })
/*=============================================== 
           8. Apps Craft Sync Slider
       ================================================*/
// function lastestArticlesSlider() {
//     if ($('#apps-craft-commentor-slider').length > 0) {

//         var sync1 = $("#apps-craft-commentor-slider");
//         var sync2 = $("#apps-craft-testimonial-thumb");

//         sync1.owlCarousel({
//             singleItem: true,
//             // slideSpeed : 1000,
//             pagination: false,
//             navigation: false,
//             addClassActive: true,
//             afterAction: syncPosition,
//             responsiveRefreshRate: 200,
//             transitionStyle: "fade",
//         });

//         $(".next").on('click', function () {
//             sync1.trigger('owl.next');
//         });
//         $(".prev").on('click', function () {
//             sync1.trigger('owl.prev');
//         });

//         sync2.owlCarousel({
//             items: 5,
//             itemsDesktop: [1199, 3],
//             itemsDesktopSmall: [979, 3],
//             itemsTablet: [768, 3],
//             itemsMobile: [479, 1],
//             pagination: false,
//             navigation: false,
//             responsiveRefreshRate: 100,
//             afterInit: function (el) {
//                 el.find(".owl-item").eq(0).addClass("synced");
//             }
//         });

//         function syncPosition(el) {
//             var current = this.currentItem;
//             $("#apps-craft-testimonial-thumb")
//                 .find(".owl-item")
//                 .removeClass("synced")
//                 .eq(current)
//                 .addClass("synced")
//             if ($("#apps-craft-testimonial-thumb").data("owlCarousel") !== undefined) {
//                 center(current)
//             }
//         }

//         $("#apps-craft-testimonial-thumb").on("click", ".owl-item", function (e) {
//             e.preventDefault();
//             var number = $(this).data("owlItem");
//             sync1.trigger("owl.goTo", number);
//         });

//         function center(number) {
//             var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
//             var num = number;
//             var found = false;
//             for (var i in sync2visible) {
//                 if (num === sync2visible[i]) {
//                     var found = true;
//                 }
//             }

//             if (found === false) {
//                 if (num > sync2visible[sync2visible.length - 1]) {
//                     sync2.trigger("owl.goTo", num - sync2visible.length + 2)
//                 } else {
//                     if (num - 1 === -1) {
//                         num = 0;
//                     }
//                     sync2.trigger("owl.goTo", num);
//                 }
//             } else if (num === sync2visible[sync2visible.length - 1]) {
//                 sync2.trigger("owl.goTo", sync2visible[1])
//             } else if (num === sync2visible[0]) {
//                 sync2.trigger("owl.goTo", num - 1)
//             }

//         }
//     } // Apps Craft Sync Slider
// }


$.getJSON('http://apiwebpublic.wisepass.co/home/topvenues', function (json) {

    var arrayTopVenueDrinking = json.data.venues;
    for (var i = 0; i < arrayTopVenueDrinking.length; i++) {
        slider(arrayTopVenueDrinking[i]);
    }
});

function slider(image) {
    var slideClone = $("#slide").clone();
    var imageEl = slideClone[0].querySelector("#imageVenue");
    imageEl.src = image.imageUrl;
    slideClone.appendTo("#customerLogos");
}
